/* gtkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gtkGLcontext.h"

#include <GL/gl.h>

#include <gdkGL/gdkGL.h>

#include <gtk/gtk.h>

static void       gtk_gl_context_realize      (GtkWidget *widget);
static void       gtk_gl_context_size_request (GtkWidget      *widget,
					       GtkRequisition *requisition);
static void       gtk_gl_context_size_allocate (GtkWidget     *widget,
						GtkAllocation *allocation);
void gtk_gl_context_activate_notify    (GtkGLContext *context);
void gtk_gl_context_inactivate_notify  (GtkGLContext *context);

enum {
  ACTIVATE_NOTIFY,
  INACTIVATE_NOTIFY,
  LAST_SIGNAL
};


static void gtk_gl_context_class_init (GtkGLContextClass *klass);
static void gtk_gl_context_init       (GtkGLContext      *gtk_gl_context);
static void gtk_gl_context_finalize   (GtkObject         *object);


static GtkWidgetClass *parent_class = NULL;
static gint gl_context_signals[LAST_SIGNAL] = { 0 };


guint
gtk_gl_context_get_type ()
{
  static guint gtk_gl_context_type = 0;

  if (!gtk_gl_context_type)
    {
      GtkTypeInfo gtk_gl_context_info =
      {
	"GtkGLContext",
	sizeof (GtkGLContext),
	sizeof (GtkGLContextClass),
	(GtkClassInitFunc) gtk_gl_context_class_init,
	(GtkObjectInitFunc) gtk_gl_context_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL,
      };

      gtk_gl_context_type = gtk_type_unique (gtk_widget_get_type (),
					     &gtk_gl_context_info);
    }

  return gtk_gl_context_type;
}

static void
gtk_gl_context_class_init (GtkGLContextClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = GTK_OBJECT_CLASS(class);
  widget_class = GTK_WIDGET_CLASS(class);

  parent_class = gtk_type_class (gtk_data_get_type ());

  gl_context_signals[ACTIVATE_NOTIFY] =
      gtk_signal_new ("activate_notify",
		      GTK_RUN_FIRST,
		      object_class->type,
		      GTK_SIGNAL_OFFSET (GtkGLContextClass,
					 activate_notify),
		      gtk_signal_default_marshaller,
		      GTK_TYPE_NONE, 0);
  gl_context_signals[INACTIVATE_NOTIFY] =
      gtk_signal_new ("inactivate_notify",
		      GTK_RUN_FIRST,
		      object_class->type,
		      GTK_SIGNAL_OFFSET (GtkGLContextClass,
					 inactivate_notify),
		      gtk_signal_default_marshaller,
		      GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, gl_context_signals, LAST_SIGNAL);

  object_class->finalize = (gpointer)gtk_gl_context_finalize;

  widget_class->realize = (gpointer)gtk_gl_context_realize;
  widget_class->size_request = (gpointer)gtk_gl_context_size_request;
  widget_class->size_allocate = (gpointer)gtk_gl_context_size_allocate;

  class->activate_notify = NULL;
  class->inactivate_notify = NULL;
}

static void
gtk_gl_context_init (GtkGLContext *gtk_gl_context)
{
  gtk_gl_context->gdk_gl_context = NULL;
}

GtkWidget*
gtk_gl_context_new (void)
{
  GtkGLContext *gtk_gl_context;

  gtk_gl_context = gtk_type_new (gtk_gl_context_get_type ());

  return GTK_WIDGET (gtk_gl_context);
}

static void
gtk_gl_context_size_request (GtkWidget      *widget,
			     GtkRequisition *requisition)
{
  requisition->width = 1;
  requisition->height = 1;
}

static void
gtk_gl_context_size_allocate (GtkWidget     *widget,
			     GtkAllocation *allocation)
{
  
}

static void
gtk_gl_context_realize (GtkWidget *widget)
{
  GdkGLContext *gdk_gl_context;
  GtkGLContext *context = GTK_GL_CONTEXT(widget);
  GdkWindowAttr attributes;
  gint attributes_mask;
  gint border_width;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GL_CONTEXT (widget));

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  border_width = GTK_CONTAINER (widget)->border_width;
  
  attributes.x = widget->allocation.x + border_width;
  attributes.y = widget->allocation.y + border_width;
  attributes.width = widget->allocation.width - 2*border_width;
  attributes.height = widget->allocation.height - 2*border_width;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget)
			| GDK_BUTTON_MOTION_MASK
			| GDK_BUTTON_PRESS_MASK
			| GDK_BUTTON_RELEASE_MASK
			| GDK_EXPOSURE_MASK
			| GDK_ENTER_NOTIFY_MASK
			| GDK_LEAVE_NOTIFY_MASK;

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, widget);

  widget->style = gtk_style_attach (widget->style, widget->window);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);

  if(!GTK_WIDGET_REALIZED (GTK_WIDGET(context)->parent))
    gtk_widget_realize(GTK_WIDGET(context)->parent);

  gdk_gl_context
      = gdk_gl_context_new (gtk_widget_get_visual (widget));
  gdk_gl_context->user_data = context;

  context->gdk_gl_context = gdk_gl_context;
}


void
gtk_gl_context_activate_notify (GtkGLContext *gtk_gl_context)
{
  gtk_signal_emit(GTK_OBJECT (gtk_gl_context),
		  gl_context_signals[ACTIVATE_NOTIFY]);
}

void
gtk_gl_context_inactivate_notify (GtkGLContext *gtk_gl_context)
{
  gtk_signal_emit(GTK_OBJECT (gtk_gl_context),
		  gl_context_signals[INACTIVATE_NOTIFY]);
}


static void
gtk_gl_context_finalize (GtkObject *object)
{
  GtkGLContext *context;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_GL_CONTEXT (object));

  context = GTK_GL_CONTEXT (object);

  if (context->gdk_gl_context)
    {
      gdk_gl_context_unref (context->gdk_gl_context);
    }

  (* GTK_OBJECT_CLASS (parent_class)->finalize) (GTK_OBJECT (context));
}
