/* gtkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <gtkGL/gtkGL.h>
#include <gdkGL/gdkGL_gdkutils.h>

typedef GSList* (* MyTestFunc) (void);

/**/

/*
 *
 */

static void
test_visual_n_colormap_expose (GtkWidget *widget,
			       GdkEvent  *event,
			       gpointer   func_data)
{
  GdkEventExpose *expose_event;

  expose_event = (GdkEventExpose *)event;

  if (expose_event->count == 0) {
    GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
      {
	glClear(GL_COLOR_BUFFER_BIT);

	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex2f(-0.8, 0.4);
	glVertex2f(-0.8, 0.1);
	glColor3f(0.0, 0.0, 1.0);
	glVertex2f(0.8, 0.1);
	glVertex2f(0.8, 0.4);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex2f(-0.8, 0.0);
	glVertex2f(-0.8, -0.3);
	glColor3f(0.0, 1.0, 0.0);
	glVertex2f(0.8, -0.3);
	glVertex2f(0.8, 0.0);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex2f(-0.8, -0.7);
	glVertex2f(-0.8, -0.4);
	glColor3f(1.0, 0.0, 0.0);
	glVertex2f(0.8, -0.4);
	glVertex2f(0.8, -0.7);
	glEnd();
      }
    GTK_GL_WRAP_END;
  }

  gtk_gl_swap_buffers_of_widget (widget);
}

static void
test_visual_n_colormap_size_allocate (GtkWidget     *widget,
				      GtkAllocation *allocation,
				      gpointer       func_data)
{
  gint width, height;

  if (GTK_WIDGET_REALIZED (widget))
    {
      width = allocation->width;
      height = allocation->height;
  
      GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
	{
	  glViewport(0, 0, (GLint)width, (GLint)height);
	}
      GTK_GL_WRAP_END;
    }
}

static GSList*
test_visual_n_colormap (void)
{
  GSList *tmp_slist = NULL;
    
  gint i;
  GList *visuals;
  GdkVisual *gdk_visual;
  GdkColormap *gdk_colormap;

  GtkWidget *window;
  GtkWidget *drawing_area;
    
  /*
   *
   */

  visuals = gdk_gl_list_visuals ();

  for (i = 0; g_list_nth (visuals, i) != NULL; ++i)
    {
      gdk_visual
	  = (GdkVisual *)(g_list_nth (visuals, i))->data;
      gdk_colormap
	  = gdk_colormap_new (gdk_visual, FALSE);

      gtk_widget_push_visual (gdk_visual);
      gtk_widget_push_colormap (gdk_colormap);
    
      window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

      gtk_window_set_title(GTK_WINDOW (window),
			   gdk_str_gdk_visual_type(gdk_visual->type));

      drawing_area = gtk_drawing_area_new ();
      gtk_gl_adapt_widget (drawing_area);

      gtk_container_add(GTK_CONTAINER(window), drawing_area);
      gtk_drawing_area_size(GTK_DRAWING_AREA (drawing_area), 400, 50);
    

      gtk_signal_connect(GTK_OBJECT(drawing_area), "size_allocate",
			 GTK_SIGNAL_FUNC
			            (test_visual_n_colormap_size_allocate),
			 NULL);
      gtk_widget_set_events(drawing_area, GDK_EXPOSURE_MASK);
      gtk_signal_connect(GTK_OBJECT(drawing_area), "expose_event",
			 GTK_SIGNAL_FUNC (test_visual_n_colormap_expose),
			 NULL);

      gtk_widget_show_all(window);

      gtk_widget_pop_colormap ();
      gtk_widget_pop_visual ();


      tmp_slist = g_slist_prepend (tmp_slist, (gpointer)window);
    }

  g_list_free (visuals); /* XXX: see gdkvisual.c */

  return tmp_slist;
}

/*
 *
 */

static void
test_nest_wrap_button_clicked (GtkButton *button,
			       gpointer   func_data)
{
  float v[4] = { 1.0, 0.5, 0.25, 0.5 };
    
  GList        *widget_list;
  GtkWidget    *widget[3];
  GtkGLContext *context[3];

  widget_list = gtk_object_get_user_data (GTK_OBJECT (button));
  
  widget[0] = g_list_nth(widget_list, 0)->data;
  widget[1] = g_list_nth(widget_list, 1)->data;
  widget[2] = g_list_nth(widget_list, 2)->data;
  context[0] = gtk_gl_context_of_widget (widget[0]);
  context[1] = gtk_gl_context_of_widget (widget[1]);
  context[2] = gtk_gl_context_of_widget (widget[2]);

  GTK_GL_WRAP_BEGIN_STRICT (context[0])
    {
      glClear(GL_COLOR_BUFFER_BIT);
      glBegin(GL_POLYGON);
      glColor3f(v[0], 0.0, 0.0);
      glVertex2f(-0.8, 0.8);

      GTK_GL_NEST_WRAP_BEGIN_STRICT (context[1])
	{
	  glClear(GL_COLOR_BUFFER_BIT);
	  glBegin(GL_POLYGON);
	  glColor3f(0.0, v[0], 0.0);
	  glVertex2f(-0.8, 0.8);
	  glColor3f(0.0, v[1], 0.0);
	  glVertex2f(-0.8, -0.8);

	  GTK_GL_NEST_WRAP_BEGIN_STRICT (context[2])
	    {
	      glClear(GL_COLOR_BUFFER_BIT);
	      glBegin(GL_POLYGON);
	      glColor3f(0.0, 0.0, v[0]);
	      glVertex2f(-0.8, 0.8);
	      glColor3f(0.0, 0.0, v[1]);
	      glVertex2f(-0.8, -0.8);

	      GTK_GL_NEST_WRAP_BEGIN_STRICT (context[0])
		{
		  glColor3f(v[1], 0.0, 0.0);
		  glVertex2f(-0.8, -0.8);
		}
	      GTK_GL_NEST_WRAP_END_STRICT (context[0]);

	      glColor3f(0.0, 0.0, v[2]);
	      glVertex2f(0.8, -0.8);
	      glColor3f(0.0, 0.0, v[3]);
	      glVertex2f(0.8, 0.8);
	      glEnd();
	    }
	  GTK_GL_NEST_WRAP_END_STRICT (context[2]);

	  glColor3f(0.0, v[2], 0.0);
	  glVertex2f(0.8, -0.8);
	  glColor3f(0.0, v[3], 0.0);
	  glVertex2f(0.8, 0.8);
	  glEnd();
	}
      GTK_GL_NEST_WRAP_END_STRICT (context[1]);

      glColor3f(v[2], 0.0, 0.0);
      glVertex2f(0.8, -0.8);
      glColor3f(v[3], 0.0, 0.0);
      glVertex2f(0.8, 0.8);
      glEnd();
    }
  GTK_GL_WRAP_END_STRICT (context[0]);

  gtk_gl_swap_buffers_of_widget (widget[0]);
  gtk_gl_swap_buffers_of_widget (widget[1]);
  gtk_gl_swap_buffers_of_widget (widget[2]);
}

static void
test_nest_wrap_size_allocate(GtkWidget     *widget,
			     GtkAllocation *allocation,
			     gpointer       func_data)
{
  gint width, height;

  if (GTK_WIDGET_REALIZED (widget))
    {
      width = allocation->width;
      height = allocation->height;

      GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
	{
	  glViewport(0, 0, (GLint)width, (GLint)height);
	}
      GTK_GL_WRAP_END;
    }
}

static void
test_nest_wrap_expose (GtkWidget *widget,
		       GdkEvent  *event,
		       gpointer   func_data)
{
  float r[4], g[4], b[4];

  gint i;
  i = (gint)func_data;

  r[0] = (1.0  * (i == 0)); g[0] = (1.0  * (i == 1)); b[0] = (1.0  * (i == 2));
  r[1] = (0.5  * (i == 0)); g[1] = (0.5  * (i == 1)); b[1] = (0.5  * (i == 2));
  r[2] = (0.25 * (i == 0)); g[2] = (0.25 * (i == 1)); b[2] = (0.25 * (i == 2));
  r[3] = (0.5  * (i == 0)); g[3] = (0.5  * (i == 1)); b[3] = (0.5  * (i == 2));
  
  GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
    {
      glClear(GL_COLOR_BUFFER_BIT);
      glBegin(GL_POLYGON);
      glColor3f(r[0], g[0], b[0]);
      glVertex2f(-0.8, 0.8);
      glColor3f(r[1], g[1], b[1]);
      glVertex2f(-0.8, -0.8);
      glColor3f(r[2], g[2], b[2]);
      glVertex2f(0.8, -0.8);
      glColor3f(r[3], g[3], b[3]);
      glVertex2f(0.8, 0.8);
      glEnd();
    }
  GTK_GL_WRAP_END;

  gtk_gl_swap_buffers_of_widget (widget);
}

static GSList*
test_nest_wrap (void)
{
  GSList *tmp_slist = NULL;

  GList *widget_list = NULL;
  gint i;
    
  GtkWidget *dialog;
  GtkWidget *test_button;

  GtkWidget *hbox;
  GtkWidget *frame;
  GtkWidget *drawing_area;
  /*
   *
   */
  
  dialog = gtk_dialog_new ();

  hbox = gtk_hbox_new (FALSE, FALSE);
  gtk_box_pack_start_defaults (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox);

  for (i = 0; i < 3; ++i) {
    char str[16];
    sprintf (str, "%d", i);
    frame = gtk_frame_new (str);
    gtk_box_pack_start_defaults (GTK_BOX (hbox), frame);
    
    drawing_area = gtk_drawing_area_new ();
    gtk_gl_adapt_widget (drawing_area);
	
    gtk_container_add (GTK_CONTAINER (frame), drawing_area);
    gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area), 80, 60);
    gtk_signal_connect (GTK_OBJECT(drawing_area), "size_allocate",
			GTK_SIGNAL_FUNC (test_nest_wrap_size_allocate),
			NULL);
    gtk_widget_set_events (drawing_area, GDK_EXPOSURE_MASK);
    gtk_signal_connect (GTK_OBJECT(drawing_area), "expose_event",
			GTK_SIGNAL_FUNC (test_nest_wrap_expose),
			(gpointer)i);
  }

  hbox = gtk_hbox_new (FALSE, FALSE);
  gtk_box_pack_start_defaults (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox);

  for (i = 0; i < 3; ++i) {
    char str[16];
    sprintf (str, "nest %d", i);
    frame = gtk_frame_new (str);
    gtk_box_pack_start_defaults (GTK_BOX(hbox), frame);
    
    drawing_area = gtk_drawing_area_new ();
    gtk_gl_adapt_widget (drawing_area);
	
    gtk_container_add (GTK_CONTAINER (frame), drawing_area);
    gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area), 80, 60);
    gtk_signal_connect (GTK_OBJECT (drawing_area), "size_allocate",
			(GtkSignalFunc)test_nest_wrap_size_allocate,
			NULL);

    widget_list = g_list_append (widget_list, (gpointer)drawing_area);
  }

  test_button = gtk_button_new_with_label ("Test");
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
		      test_button, TRUE, TRUE, 0);
  gtk_object_set_user_data (GTK_OBJECT (test_button), (gpointer)widget_list);
  gtk_signal_connect (GTK_OBJECT(test_button), "clicked",
		      (GtkSignalFunc)test_nest_wrap_button_clicked,
		      NULL);

  gtk_widget_show_all (dialog);


  tmp_slist = g_slist_prepend (tmp_slist, (gpointer)dialog);
  
  return tmp_slist;
}



/*
 *
 */

static GSList*
test_wrap_macro (void)
{
  GSList *tmp_slist = NULL;

  GtkWidget *window1;
  GtkWidget *window2;
  GtkGLContext *context1;
  GtkGLContext *context2;


  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  window2 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_gl_adapt_widget (window1);
  gtk_gl_adapt_widget (window2);
  gtk_widget_show (window1);
  gtk_widget_show (window2);

  context1 = gtk_gl_context_of_widget (window1);
  context2 = gtk_gl_context_of_widget (window2);
  
  GTK_GL_WRAP_BEGIN (context1)
    {
    }
  GTK_GL_WRAP_END;

  GTK_GL_WRAP_BEGIN (context1)
    {
      GTK_GL_NEST_WRAP_BEGIN (context2)
	{
	}
      GTK_GL_NEST_WRAP_END;
    }
  GTK_GL_WRAP_END;

  GTK_GL_WRAP_BEGIN_STRICT (context1)
    {
    }
  GTK_GL_WRAP_END_STRICT (context1);

  GTK_GL_WRAP_BEGIN_STRICT (context1)
    {
      GTK_GL_NEST_WRAP_BEGIN_STRICT (context2)
	{
	}
      GTK_GL_NEST_WRAP_END_STRICT (context2);
    }
  GTK_GL_WRAP_END_STRICT (context1);

  gtk_gl_push_context (context1);
  GTK_GL_WRAP_BEGIN_DEFAULTS
    {
    }
  GTK_GL_WRAP_END_DEFAULTS;
  gtk_gl_pop_context ();

  gtk_gl_push_context (context1);
  GTK_GL_WRAP_BEGIN_DEFAULTS
    {
      gtk_gl_push_context (context2);
      GTK_GL_NEST_WRAP_BEGIN_DEFAULTS
	{
	}
      GTK_GL_NEST_WRAP_END_DEFAULTS;
      gtk_gl_pop_context ();
    }
  GTK_GL_WRAP_END_DEFAULTS;
  gtk_gl_pop_context ();

  GTK_GL_WRAP (context1, {});

  GTK_GL_WRAP (context1,
	       {
		 GTK_GL_NEST_WRAP (context2, {});
	       });

  gtk_gl_push_context (context1);
  GTK_GL_WRAP_DEFAULTS ({});
  gtk_gl_pop_context ();

  gtk_gl_push_context (context1);
  GTK_GL_WRAP_DEFAULTS ({
			  gtk_gl_push_context (context2);
			  GTK_GL_NEST_WRAP_DEFAULTS ({});
			  gtk_gl_pop_context ();
			});
  gtk_gl_pop_context ();

  tmp_slist = g_slist_prepend (tmp_slist, window1);
  tmp_slist = g_slist_prepend (tmp_slist, window2);

  return tmp_slist;
}

/***/
/***/
/***/

static void
test_dialog_destroy (GtkWidget *widget,
		     gpointer   func_data)
{
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (func_data),
			       GTK_STATE_NORMAL);
}

static void
test_dialog_delete (GtkWidget *widget,
		    GdkEvent  *event,
		    gpointer   func_data)
{
  gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (func_data),
			       GTK_STATE_NORMAL);
}

static void
test_dialogs_setup (GSList    *dialogs_slist,
		    GtkWidget *toggle_button)
{
  GSList *tmp_slist;
  GtkWidget *dialog;
  
  while (dialogs_slist != NULL)
    {
      tmp_slist = dialogs_slist;
      dialogs_slist = g_slist_next (dialogs_slist);

      dialog = GTK_WIDGET (tmp_slist->data);
      
      gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			  GTK_SIGNAL_FUNC (test_dialog_destroy),
			  (gpointer)toggle_button);
      gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
			  GTK_SIGNAL_FUNC (test_dialog_delete),
			  (gpointer)toggle_button);
    }
}

static void
test_dialogs_destroy (GSList *dialogs_slist)
{
  GSList *tmp_slist;
  GtkWidget *dialog;

  while (dialogs_slist != NULL)
    {
      tmp_slist = dialogs_slist;
      dialogs_slist = g_slist_next (dialogs_slist);

      dialog = GTK_WIDGET (tmp_slist->data);
      gtk_widget_hide (dialog);
      gtk_widget_destroy (dialog);

      g_slist_free_1 (tmp_slist);
    }
}

static void
test_toggle (GtkToggleButton *toggle_button,
	     gpointer         func_data)
{
  GSList *slist;
  MyTestFunc test_func;

  slist = gtk_object_get_user_data (GTK_OBJECT (toggle_button));

  if (slist == NULL)
    {
      test_func = (MyTestFunc)func_data;
      slist = (* test_func) ();
      gtk_object_set_user_data (GTK_OBJECT (toggle_button), (gpointer)slist);
    }

  if (toggle_button->active)
    {
      test_dialogs_setup (slist, GTK_WIDGET (toggle_button));
    }
  else
    {
      test_dialogs_destroy (slist);
      gtk_object_set_user_data (GTK_OBJECT (toggle_button), NULL);
    }
}

static void
create_main_dialog (void)
{
  struct {
    gchar      *label_text;
    MyTestFunc  func;
  } tests[] =
    {
      { "Visual & Colormap", test_visual_n_colormap },
      { "Nest Wrap", test_nest_wrap },
      { "Wrap Macros", test_wrap_macro },
    };
  gint num_tests = sizeof (tests) / sizeof (tests[0]);

  GtkWidget *dialog;
  GtkWidget *quit_button;
  GtkWidget *button;
  gint i;

  dialog = gtk_dialog_new ();
  quit_button = gtk_button_new_with_label ("Quit");
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
		     quit_button, TRUE, TRUE, 0);

  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit),
		      NULL);
  gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
		      GTK_SIGNAL_FUNC (gtk_main_quit),
		      NULL);
  gtk_signal_connect (GTK_OBJECT (quit_button), "clicked",
		      GTK_SIGNAL_FUNC (gtk_main_quit),
		      NULL);

  for (i = 0; i < num_tests; ++i)
    {
      button = gtk_toggle_button_new_with_label (tests[i].label_text);
      gtk_signal_connect (GTK_OBJECT (button), "toggled",
			  GTK_SIGNAL_FUNC (test_toggle),
			  (gpointer)tests[i].func);
      gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
			  button, TRUE, TRUE, 0);
    }

  gtk_widget_show_all (dialog);
}

int
main (int    argc,
      char **argv)
{
  gtk_init (&argc, &argv);
  /* gtk_rc_parse (""); */

  gtk_gl_init (&argc, &argv);

  create_main_dialog ();

  gtk_main ();

  return 0;
}
