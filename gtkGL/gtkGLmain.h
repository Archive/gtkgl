/* gtkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GTK_GL_MAIN_H__
#define __GTK_GL_MAIN_H__


#include <gdkGL/gdkGL.h>
#include <gtkGL/gtkGLcontext.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


void          gtk_gl_context_push    (GtkGLContext *gtk_gl_context);
void          gtk_gl_context_pop     (void);
GtkGLContext* gtk_gl_context_get_top (void);

void          gtk_gl_context_swap_buffers (GtkGLContext *gtk_gl_context);

/***/

#define GTK_GL_WRAP_BEGIN_STRICT(context)		          \
     {							          \
       if (gtk_gl_wrap_begin_strict (context) != TRUE)	          \
	 {						          \
	   g_warning ("GTK_GL_WRAP_BEGIN_STRICT failed\n");       \
	 }						          \
       else						          \
	 {
#define GTK_GL_WRAP_END_STRICT(context)			          \
	   gtk_gl_wrap_end_strict (context);		          \
	 }						          \
     }

#define GTK_GL_NEST_WRAP_BEGIN_STRICT(context)		          \
     {							          \
       if (gtk_gl_nest_wrap_begin_strict (context) != TRUE)       \
	 {						          \
	   g_warning ("GTK_GL_NEST_WRAP_BEGIN_STRICT failed\n");  \
	 }						          \
       else						          \
	 {
#define GTK_GL_NEST_WRAP_END_STRICT(context)		          \
	   gtk_gl_nest_wrap_end_strict (context);	          \
	 }						          \
     }

#define GTK_GL_WRAP_BEGIN(context)				\
     {								\
       gtk_gl_push_context (context);                   	\
       if (gtk_gl_wrap_begin_defaults () != TRUE)		\
	 {							\
	   g_warning ("GTK_GL_WRAP_BEGIN failed\n");    	\
	 }							\
       else							\
	 {
#define GTK_GL_WRAP_END						\
	   gtk_gl_wrap_end_defaults ();				\
	 }							\
       gtk_gl_pop_context ();                           	\
     }

#define GTK_GL_NEST_WRAP_BEGIN(context)				\
     {								\
       gtk_gl_push_context (context);                           \
       if (gtk_gl_nest_wrap_begin_defaults () != TRUE)		\
	 {							\
	   g_warning ("GTK_GL_NEST_WRAP_BEGIN failed\n");       \
	 }							\
       else							\
	 {
#define GTK_GL_NEST_WRAP_END					\
	   gtk_gl_nest_wrap_end_defaults ();			\
	 }							\
       gtk_gl_pop_context ();                                   \
     }

#define GTK_GL_WRAP_BEGIN_DEFAULTS				\
     {								\
       if (gtk_gl_wrap_begin_defaults () != TRUE)		\
	 {							\
	   g_warning ("GTK_GL_WRAP_BEGIN_DEFAULTS failed\n");   \
	 }							\
       else							\
	 {
#define GTK_GL_WRAP_END_DEFAULTS				\
	   gtk_gl_wrap_end_defaults ();				\
	 }							\
     }

gint          gtk_gl_wrap_begin_strict        (GtkGLContext *context);
gint          gtk_gl_wrap_end_strict          (GtkGLContext *context);
gint          gtk_gl_nest_wrap_begin_strict   (GtkGLContext *context);
gint          gtk_gl_nest_wrap_end_strict     (GtkGLContext *context);
gint          gtk_gl_wrap_begin_defaults      (void);
gint          gtk_gl_wrap_end_defaults        (void);
gint          gtk_gl_nest_wrap_begin_defaults (void);
gint          gtk_gl_nest_wrap_end_defaults   (void);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_GL_MAIN_H__ */
