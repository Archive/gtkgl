/* gtkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GTK_GL_CONTEXT_H__
#define __GTK_GL_CONTEXT_H__


#include <gtk/gtkdata.h>
#include <gtk/gtkwidget.h>

#include <gdkGL/gdkGL.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_GL_CONTEXT(obj)          GTK_CHECK_CAST (obj, gtk_gl_context_get_type (), GtkGLContext)
#define GTK_GL_CONTEXT_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_gl_context_get_type (), GtkGLContextClass)
#define GTK_IS_GL_CONTEXT(obj)       GTK_CHECK_TYPE (obj, gtk_gl_context_get_type ())


typedef struct _GtkGLContext       GtkGLContext;
typedef struct _GtkGLContextClass  GtkGLContextClass;

struct _GtkGLContext
{
  GtkWidget parent;

  GdkGLContext *gdk_gl_context;
};

struct _GtkGLContextClass
{
  GtkWidgetClass parent_class;

  void (* activate_notify) (GtkGLContext *context);
  void (* inactivate_notify) (GtkGLContext *context);
};

guint      gtk_gl_context_get_type     (void);
GtkWidget* gtk_gl_context_new          (void);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_GL_CONTEXT_H__ */
