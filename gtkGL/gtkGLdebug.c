/* gtkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gtkGLdebug.h"

#include <stdlib.h>
#include <string.h>


guint gtk_gl_debug_flags = 0;


#ifdef G_ENABLE_DEBUG

static guint gtk_gl_parse_debug_string (const gchar *string);


static GDebugKey gtk_gl_debug_keys[] = {
  {"misc", GTK_GL_DEBUG_MISC},
};
static const int gtk_gl_ndebug_keys
                               = sizeof(gtk_gl_debug_keys) / sizeof(GDebugKey);


void
gtk_gl_debug_flags_init (int    *argc,
			 char ***argv)
{
  gint   i;
  gchar *debug_string;
  GList *tmp;
  GList *arg;
  GList *args;
  gchar *equal_position;


  args = NULL;
  for (i = 0; i < *argc; ++i)
    args = g_list_append (args, (gpointer)((*argv)[i]));


  debug_string = getenv("GTK_GL_DEBUG");

  if (debug_string != NULL)
    gtk_gl_debug_flags = gtk_gl_parse_debug_string (debug_string);


  tmp = g_list_next (args);
  while (tmp != NULL)
    {
      arg = tmp;
      tmp = g_list_next (tmp);

      if ((strcmp ("--gtk-gl-debug", arg->data)  == 0) ||
	  (strncmp ("--gtk-gl-debug=", arg->data, 15) == 0))
	{
	  equal_position = strchr (arg->data, '=');
	  arg->data = NULL;

	  if (equal_position != NULL)
	    {
	      debug_string = equal_position + 1;
	    }
	  else if (tmp != NULL)
	    {
	      arg = tmp;
	      tmp = g_list_next (tmp);

	      debug_string = (gchar *)arg->data;
	      arg->data = NULL;
	    }
	  else
	    {
	      debug_string = NULL;
	    }

	  if (debug_string != NULL)
	    {
	      gtk_gl_debug_flags |= gtk_gl_parse_debug_string (debug_string);
	    }
	}
      else if ((strcmp ("--gtk-gl-no-debug", arg->data) == 0) ||
	       (strncmp ("--gtk-gl-no-debug=", arg->data, 18) == 0))
	{
	  gchar *equal_position = strchr (arg->data, '=');
	  arg->data = NULL;

	  if (equal_position != NULL)
	    {
	      debug_string = equal_position + 1;
	    }
	  else if (tmp != NULL)
	    {
	      arg = tmp;
	      tmp = g_list_next (tmp);

	      debug_string = (gchar *)arg->data;
	      arg->data = NULL;
	    }
	  else
	    {
	      debug_string = NULL;
	    }

	  if (debug_string != NULL)
	    {
	      gtk_gl_debug_flags &= ~gtk_gl_parse_debug_string (debug_string);
	    }
	}
    }

  while (args != NULL)
    {
      arg = args;
      args = g_list_next (args);

      if (arg->data == NULL)
	{
	  args = g_list_remove_link (args, arg);
	  g_list_free_1 (arg);
	}
    }

  i = 0;
  while (args != NULL)
    {
      arg = args;
      args = g_list_next (args);

      (*argv)[i] = (char *)arg->data;

      ++i;
    }
  (*argc) = g_list_length (args);


  g_list_free (args);
}

static guint
gtk_gl_parse_debug_string (const gchar *string)
{
  return g_parse_debug_string (string,
			       gtk_gl_debug_keys,
			       gtk_gl_ndebug_keys);
}
#endif /* G_ENABLE_DEBUG */
