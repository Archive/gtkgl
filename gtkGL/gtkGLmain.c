/* gtkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "gtkGLmain.h"
#include "gtkGLcontext.h"

/* These two are in gtkGLcontext.c as well */
void gtk_gl_context_activate_notify    (GtkGLContext *context);
void gtk_gl_context_inactivate_notify  (GtkGLContext *context);

static GtkGLContext* gtk_gl_get_second_context (void);

static GSList *gtk_gl_stack_gtk_gl_context = NULL;

static const gchar *gtk_gl_context_key = "gtk-gl-context";

void
gtk_gl_context_swap_buffers (GtkGLContext *gtk_gl_context)
{
  g_return_if_fail (gtk_gl_context != NULL);
  g_return_if_fail (GTK_IS_GL_CONTEXT (gtk_gl_context));
  g_return_if_fail (GTK_WIDGET_REALIZED (gtk_gl_context));

  gdk_gl_swap_buffers (GTK_WIDGET(gtk_gl_context)->window);
}

void
gtk_gl_context_push (GtkGLContext *gtk_gl_context)
{
  g_return_if_fail (gtk_gl_context != NULL);

  gtk_gl_stack_gtk_gl_context
      = g_slist_prepend(gtk_gl_stack_gtk_gl_context, gtk_gl_context);
}

void
gtk_gl_context_pop (void)
{
  GSList *tmp;

  if (gtk_gl_stack_gtk_gl_context != NULL)
    {
      tmp = gtk_gl_stack_gtk_gl_context;
      gtk_gl_stack_gtk_gl_context = tmp->next;
      g_slist_free_1(tmp);
    }
}

GtkGLContext*
gtk_gl_context_get_top (void)
{
  if (gtk_gl_stack_gtk_gl_context != NULL)
    return gtk_gl_stack_gtk_gl_context->data;
  else
    return NULL;
}

static GtkGLContext*
gtk_gl_get_second_context (void)
{
  GtkGLContext *second_context;

  if ((gtk_gl_stack_gtk_gl_context != NULL) &&
      (g_slist_next (gtk_gl_stack_gtk_gl_context) != NULL))
    second_context = g_slist_next (gtk_gl_stack_gtk_gl_context)->data;
  else
    second_context = NULL;

  return second_context;
}

gint
gtk_gl_wrap_begin_strict (GtkGLContext *context)
{
  GdkGLContext *gdk_gl_context;
  GdkWindow    *gdk_window;
  gint r;
    
  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GL_CONTEXT (context), FALSE);
  g_return_val_if_fail (gtk_gl_context_get_top () == NULL, FALSE);

  if (context->gdk_gl_context == NULL)
    gtk_widget_realize (GTK_WIDGET(context));

  gdk_gl_context = context->gdk_gl_context;
  gdk_window = GTK_WIDGET(context)->window;

  r = gdk_gl_set_current (gdk_gl_context, gdk_window);

  if (r == TRUE)
    {
      gtk_gl_context_push (context);
      gtk_gl_context_activate_notify (context);
    }

  return r;
}

gint
gtk_gl_wrap_end_strict (GtkGLContext *context)
{
  gint r;
    
  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GL_CONTEXT (context), FALSE);
  g_return_val_if_fail (gtk_gl_context_get_top () == context, FALSE);
  g_return_val_if_fail (gtk_gl_get_second_context () == NULL, FALSE);

  gtk_gl_context_inactivate_notify (context);
  gtk_gl_context_pop ();

  r = TRUE;

  return r;
}

gint
gtk_gl_nest_wrap_begin_strict (GtkGLContext *context)
{
  GdkGLContext *gdk_gl_context;
  GdkWindow    *gdk_window;
  gint r;

  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GL_CONTEXT (context), FALSE);
  g_return_val_if_fail (gtk_gl_context_get_top () != NULL, FALSE);

  if (context->gdk_gl_context == NULL)
    gtk_widget_realize (GTK_WIDGET(context));

  gdk_gl_context = context->gdk_gl_context;
  gdk_window = GTK_WIDGET(context)->window;

  r = gdk_gl_set_current (gdk_gl_context, gdk_window);

  if (r == TRUE)
    {
      gtk_gl_context_inactivate_notify (gtk_gl_context_get_top ());
      gtk_gl_context_push (context);
      gtk_gl_context_activate_notify (context);
    }

  return r;
}

gint
gtk_gl_nest_wrap_end_strict (GtkGLContext *context)
{
  GtkGLContext *tmp_context;
  gint r;
    
  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GL_CONTEXT (context), FALSE);
  g_return_val_if_fail (gtk_gl_context_get_top () == context, FALSE);
  g_return_val_if_fail (gtk_gl_get_second_context () != NULL, FALSE);

  gtk_gl_context_inactivate_notify (context);
  gtk_gl_context_pop ();
  
  tmp_context = gtk_gl_context_get_top ();

  if (tmp_context != NULL)
    {
      if (gdk_gl_set_current (tmp_context->gdk_gl_context,
			      GTK_WIDGET(tmp_context)->window) == FALSE)
	{
	  gdk_gl_unset_current ();
	}
      else
	{
	  gtk_gl_context_activate_notify (tmp_context);
	}
      r = TRUE;
    }
  else
    {
      /* if reach here, maybe gtk_gl_get_second_context has bugs */
      g_assert_not_reached ();

      r = FALSE;
    }

  return r;
}

gint
gtk_gl_wrap_begin_defaults (void)
{
  GtkGLContext *context;
  GdkGLContext *gdk_gl_context;
  GdkWindow    *gdk_window;
  gint r;

  context = gtk_gl_context_get_top ();
    
  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GL_CONTEXT (context), FALSE);
  g_return_val_if_fail (gtk_gl_get_second_context () == NULL, FALSE);


  if (context->gdk_gl_context == NULL)
    gtk_widget_realize(GTK_WIDGET(context));

  gdk_gl_context = context->gdk_gl_context;
  gdk_window = GTK_WIDGET(context)->window;

  r = gdk_gl_set_current (gdk_gl_context, gdk_window);

  return r;
}

gint
gtk_gl_wrap_end_defaults (void)
{
  GtkGLContext *context;

  context = gtk_gl_context_get_top ();

  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GL_CONTEXT (context), FALSE);
  g_return_val_if_fail (gtk_gl_get_second_context () == NULL, FALSE);

  return TRUE;
}


gint
gtk_gl_nest_wrap_begin_defaults (void)
{
  GtkGLContext *context;
  GdkGLContext *gdk_gl_context;
  GdkWindow    *gdk_window;
  gint r;

  context = gtk_gl_context_get_top ();
    
  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GL_CONTEXT (context), FALSE);
  g_return_val_if_fail (gtk_gl_get_second_context () != NULL, FALSE);


  if (context->gdk_gl_context == NULL)
    gtk_widget_realize(GTK_WIDGET(context));

  gdk_gl_context = context->gdk_gl_context;
  gdk_window = GTK_WIDGET(context)->window;

  r = gdk_gl_set_current (gdk_gl_context, gdk_window);

  return r;
}

gint
gtk_gl_nest_wrap_end_defaults (void)
{
  GtkGLContext *context;

  context = gtk_gl_context_get_top ();

  g_return_val_if_fail (context != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GL_CONTEXT (context), FALSE);
  g_return_val_if_fail (gtk_gl_get_second_context () != NULL, FALSE);

  return TRUE;
}
