/* gdkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gdk/gdkx.h>
#include <gdk/gdkprivate.h>

#include "gdkGL_gdkutils.h"
#include "gdkGL_gdkxutils.h"


GdkVisual *
gdk_colormap_get_visual(GdkColormap *gdk_colormap)
{
  g_return_val_if_fail(gdk_colormap != NULL, (GdkVisual *)NULL);

  return (((GdkColormapPrivate*) gdk_colormap)->visual);
}

gchar *
gdk_str_gdk_visual_type(GdkVisualType gdk_visual_type)
{
  switch (gdk_visual_type)
    {
    case GDK_VISUAL_STATIC_GRAY:
      return "GDK_VISUAL_STATIC_GRAY";
      break;
    case GDK_VISUAL_GRAYSCALE:
      return "GDK_VISUAL_GRAYSCALE";
      break;
    case GDK_VISUAL_STATIC_COLOR:
      return "GDK_VISUAL_STATIC_COLOR";
      break;
    case GDK_VISUAL_PSEUDO_COLOR:
      return "GDK_VISUAL_PSEUDO_COLOR";
      break;
    case GDK_VISUAL_TRUE_COLOR:
      return "GDK_VISUAL_TRUE_COLOR";
      break;
    case GDK_VISUAL_DIRECT_COLOR:
      return "GDK_VISUAL_DIRECT_COLOR";
      break;
    default:
      return (gchar *)NULL;
      break;
    }
}

gchar *
gdk_str_gdk_byte_order(GdkByteOrder byte_order)
{
  switch (byte_order)
    {
    case GDK_LSB_FIRST:
      return "GDK_LSB_FIRST";
      break;
    case GDK_MSB_FIRST:
      return "GDK_MSB_FIRST";
      break;
    default:
      return (gchar *)NULL;
      break;
    }
}

#if 0
#include <stdarg.h>

gint
gdk_examine_visual (GdkVisual *visual,
		    ...)
{
  va_list args;
  gint    nargs;
  gchar   *attrib_name;
  gint    attrib_req;
  gint    i;
  gint    r;

  nargs = 0;
  va_start (args, visual);
  while (va_arg (args, gchar *) != NULL)
    {
      (void) va_arg (args, gint);
      ++nargs;
    }
  va_end (args);

  r = TRUE;
  va_start (args, visual);
  for (i = 0; i < nargs; ++i)
    {
      attrib_name = va_arg (args, gchar *);
      attrib_req = va_arg (args, gint);

      if (0)
	{}
      else if (strcmp ("type", attrib_name) == 0)
	{
	  if (visual->type != attrib_req)
	    {
	      r = FALSE;
	      break;
	    }
	}
      else if (strcmp ("depth", attrib_name) == 0)
	{
	  if (visual->depth < attrib_req)
	    {
	      r = FALSE;
	      break;
	    }
	}
      else if (strcmp ("byte_order", attrib_name) == 0)
	{
	  if (visual->byte_order != attrib_req)
	    {
	      r = FALSE;
	      break;
	    }
	}
      else if (strcmp ("colormap_size", attrib_name) == 0)
	{
	  if (visual->colormap_size < attrib_req)
	    {
	      r = FALSE;
	      break;
	    }
	}
      else if (strcmp ("bits_per_rgb", attrib_name) == 0)
	{
	  if (visual->bits_per_rgb < attrib_req)
	    {
	      r = FALSE;
	      break;
	    }
	}
      else if (strcmp ("red_prec", attrib_name) == 0)
	{
	  if (visual->red_prec < attrib_req)
	    {
	      r = FALSE;
	      break;
	    }
	}
      else if (strcmp ("green_prec", attrib_name) == 0)
	{
	  if (visual->green_prec < attrib_req)
	    {
	      r = FALSE;
	      break;
	    }
	}
      else if (strcmp ("blue_prec", attrib_name) == 0)
	{
	  if (visual->blue_prec < attrib_req)
	    {
	      r = FALSE;
	      break;
	    }
	}
      else
	{
	  g_warning ("'%s' is unknown\n", attrib_name);
	}
    }
  va_end (args);

  return r;
}

void
gdk_print_visual (GdkVisual *visual)
{
  g_print ("%x: %s   (%s)\n"
	   "  depth: %d   bits_per_rgb: %d  colormap_size: %d\n"
	   "    mask:  0x%04x 0x%04x 0x%04x\n"
	   "    shift: %-6d %-6d %-6d\n"
	   "    prec:  %-6d %-6d %-6d\n",
	   visual,
	   gdk_str_gdk_visual_type (visual->type),
	   gdk_str_gdk_byte_order (visual->byte_order),
	   visual->depth, visual->bits_per_rgb, visual->colormap_size,
	   visual->red_mask, visual->green_mask, visual->blue_mask,
	   visual->red_shift, visual->green_shift, visual->blue_shift,
	   visual->red_prec, visual->green_prec, visual->blue_prec);
}
#endif


Display*
x_display_of_gdk_visual (GdkVisual *gdk_visual)
{
  return (Display *)GDK_DISPLAY(); /* XXX */
}

XVisualInfo*
x_visual_info_new_from_gdk_visual (GdkVisual *gdk_visual)
{
  XVisualInfo *x_visual_info;
  Display *x_display;

  g_return_val_if_fail (gdk_visual != NULL, (XVisualInfo *)NULL);


  x_display = x_display_of_gdk_visual (gdk_visual);
  x_visual_info = g_new (XVisualInfo, 1);

  x_visual_info->visual        = (Visual *)GDK_VISUAL_XVISUAL (gdk_visual);

  x_visual_info->visualid      = XVisualIDFromVisual (x_visual_info->visual);
  x_visual_info->screen        = DefaultScreen (x_display); /* XXX */

  x_visual_info->class         = x_visual_class_from_gdk_visual_type
				      (gdk_visual->type);
  x_visual_info->depth         = gdk_visual->depth;
  x_visual_info->red_mask      = gdk_visual->red_mask;
  x_visual_info->green_mask    = gdk_visual->green_mask;
  x_visual_info->blue_mask     = gdk_visual->blue_mask;
  x_visual_info->colormap_size = gdk_visual->colormap_size;
  x_visual_info->bits_per_rgb  = gdk_visual->bits_per_rgb;

  return x_visual_info;
}

int
x_visual_class_from_gdk_visual_type (GdkVisualType gdk_visual_type)
{
  int x_visual_class;

  x_visual_class = NULL;

  switch (gdk_visual_type)
    {
    case GDK_VISUAL_STATIC_GRAY:
      x_visual_class	= StaticGray;
      break;
    case GDK_VISUAL_GRAYSCALE:
      x_visual_class	= GrayScale;
      break;
    case GDK_VISUAL_STATIC_COLOR:
      x_visual_class	= StaticColor;
      break;
    case GDK_VISUAL_PSEUDO_COLOR:
      x_visual_class	= PseudoColor;
      break;
    case GDK_VISUAL_TRUE_COLOR:
      x_visual_class	= TrueColor;
      break;
    case GDK_VISUAL_DIRECT_COLOR:
      x_visual_class	= DirectColor;
      break;
    default:
      g_assert_not_reached ();
    }

  return x_visual_class;
}
