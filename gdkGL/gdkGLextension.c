/* gdkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gdk/gdkx.h>

#include <GL/gl.h>
#include <GL/glx.h>

#include "gdkGL.h"

#include <string.h>


static GList *gdk_gl_extensions = NULL;


GList*
gdk_gl_list_extensions (void)
{
  Display    *x_display;
  int        x_screen_no;
  const char *glx_extensions_string;
  gchar      *extensions_string;
  gchar      *p;
  GList      *tmp;
  GList      *extensions;

  if (gdk_gl_extensions == NULL)
    {
      x_display   = (Display *)GDK_DISPLAY();
      x_screen_no = (int)gdk_screen;

      /* XXX: FIXME:  glXQueryExtensionsString() is appeared in GLX 1.1  */
      /*              maybe need check GLX version before use it.        */

      glx_extensions_string
	               = glXQueryExtensionsString (x_display, x_screen_no);

      extensions_string = g_strdup (glx_extensions_string);

      p = extensions_string;
      while ((p = strchr (p, ' ')) != NULL)
	{
	  *p = '\0';
	  ++p;
	}

      p = extensions_string;
      while (strlen (p) > 0)
	{
	  gdk_gl_extensions = g_list_append (gdk_gl_extensions,
					     (gpointer)g_strdup (p));
	  p += strlen (p) + 1;
	}

      g_free (extensions_string);
    }

  extensions = NULL;
  tmp = gdk_gl_extensions;
  while (tmp != NULL)
    {
      extensions = g_list_append (extensions, tmp->data);
      tmp = g_list_next (tmp);
    }

  return extensions;
}
