/* gdkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gdk/gdkx.h>

#include "gdkGL.h"
#include "gdkGLprivate.h"
#include "gdkGL_gdkxutils.h"


static void       gdk_gl_context_init    (GdkGLContext *gdk_gl_context);
static void       gdk_gl_context_destroy (GdkGLContext *gdk_gl_context);
static GLXContext glx_context_new_full   (GdkVisual    *gdk_visual,
					  gint          direct_rendering,
					  GdkGLContext *fellow_gdk_gl_context);


static void
gdk_gl_context_init (GdkGLContext *gdk_gl_context)
{
#define g_list_alloc_with_data(data) g_list_append (NULL, (data))

  GdkGLContextPrivate *private;

  g_return_if_fail (gdk_gl_context != NULL);


  private = (GdkGLContextPrivate *)gdk_gl_context;

  private->gdk_gl_context.user_data = NULL;

  private->glx_context = NULL;
  private->gdk_visual  = NULL;
  private->xxx___share = g_list_alloc_with_data ((gpointer)private);
  private->ref_count   = 0;

#undef g_list_alloc_with_data
}

GdkGLContext*
gdk_gl_context_new (GdkVisual *gdk_visual)
{
  return gdk_gl_context_new_full (gdk_visual, TRUE, NULL);
}

GdkGLContext*
gdk_gl_context_new_full (GdkVisual    *gdk_visual,
			 gint          direct_rendering,
			 GdkGLContext *fellow_gdk_gl_context)
{
  GdkGLContextPrivate *private;
  GLXContext glx_context;

  g_return_val_if_fail (gdk_visual != NULL,                       NULL);
  g_return_val_if_fail (gdk_gl_check_visual (gdk_visual) == TRUE, NULL);


  glx_context = glx_context_new_full (gdk_visual,
				      direct_rendering,
				      fellow_gdk_gl_context);

  if (glx_context != NULL)
    {
      private = g_new(GdkGLContextPrivate, 1);
      gdk_gl_context_init ((GdkGLContext *)private);

      private->gdk_visual  = gdk_visual;
      private->glx_context = glx_context;
      private->ref_count   = 1;

      if (fellow_gdk_gl_context != NULL)
	{
	  GdkGLContextPrivate *fellow_private;
	  fellow_private = (GdkGLContextPrivate *)fellow_gdk_gl_context;

	  g_list_concat (       private->xxx___share,
			 fellow_private->xxx___share);
	}
    }
  else
    {
      private = NULL;
    }

  return (GdkGLContext *)private;
}

static void
gdk_gl_context_destroy (GdkGLContext *gdk_gl_context)
{
  GdkGLContextPrivate *private;
  Display *x_display;

  private = (GdkGLContextPrivate *)gdk_gl_context;

  g_return_if_fail (gdk_gl_context != NULL);
  g_return_if_fail (private->ref_count == 0);


  x_display = x_display_of_gdk_visual (private->gdk_visual);
  glXDestroyContext (x_display, private->glx_context);

  g_list_remove (private->xxx___share, (gpointer)private);

  g_free(private);
}

GdkGLContext *
gdk_gl_context_ref (GdkGLContext *gdk_gl_context)
{
  GdkGLContextPrivate *private;
    
  g_return_val_if_fail (gdk_gl_context != NULL, NULL);


  private = (GdkGLContextPrivate *)gdk_gl_context;

  private->ref_count += 1;

  return (GdkGLContext *)private;
}

void
gdk_gl_context_unref (GdkGLContext *gdk_gl_context)
{
  GdkGLContextPrivate *private;

  g_return_if_fail (gdk_gl_context != NULL);


  private = (GdkGLContextPrivate *)gdk_gl_context;

  private->ref_count -= 1;

  if (private->ref_count == 0)
    gdk_gl_context_destroy ((GdkGLContext *)private);
}

GdkVisual*
gdk_gl_context_get_visual (GdkGLContext *gdk_gl_context)
{
  GdkGLContextPrivate *private;
  GdkVisual *gdk_visual;

  g_return_val_if_fail (gdk_gl_context != NULL, NULL);


  private = (GdkGLContextPrivate *)gdk_gl_context;

  gdk_visual = private->gdk_visual;

  return gdk_visual;
}


GLXContext
glx_context_of_gdk_gl_context (GdkGLContext *gdk_gl_context)
{
  GdkGLContextPrivate *private;
  GLXContext glx_context;


  if (gdk_gl_context != NULL)
    {
      private = (GdkGLContextPrivate *)gdk_gl_context;

      glx_context = private->glx_context;
    }
  else
    {
      glx_context = NULL;
    }

  return glx_context;
}



static GLXContext
glx_context_new_full (GdkVisual    *gdk_visual,
		      gint          direct_rendering,
		      GdkGLContext *fellow_gdk_gl_context)
{
  GLXContext   glx_context;
  Display     *x_display;
  XVisualInfo *x_visual_info;
  GLXContext   fellow_glx_context;

  g_return_val_if_fail (gdk_visual != NULL, NULL);


  x_display          = x_display_of_gdk_visual (gdk_visual);
  x_visual_info      = x_visual_info_new_from_gdk_visual (gdk_visual);
  fellow_glx_context = glx_context_of_gdk_gl_context (fellow_gdk_gl_context);

  glx_context = glXCreateContext (x_display,
				  x_visual_info,
				  fellow_glx_context,
				  (direct_rendering == TRUE) ? True : False);

  g_free (x_visual_info);

  return glx_context;
}
