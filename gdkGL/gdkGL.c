/* gdkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gdk/gdkx.h>

#include "gdkGL.h"
#include "gdkGLprivate.h"
#include "gdkGLdebug.h"


static gint gdk_gl_check_glx (void);


static GdkGLContext *gdk_gl_current_gdk_gl_context = NULL;
static GdkDrawable  *gdk_gl_current_gdk_drawable = NULL;


void
gdk_gl_init (int    *argc,
	     char ***argv)
{
#ifdef G_ENABLE_DEBUG
  gdk_gl_debug_flags_init (argc, argv);
#endif /* G_ENABLE_DEBUG */

  gdk_gl_check_glx ();
}

static gint
gdk_gl_check_glx (void)
{
  Display *x_display;
  Bool has_glx_extension;
  Bool got_glx_version;
  int glx_major_version;
  int glx_minor_version;
  int ignore;


  x_display = (Display *)GDK_DISPLAY();

  has_glx_extension = glXQueryExtension (x_display, &ignore, &ignore);
  if (has_glx_extension == True)
    {
      got_glx_version = glXQueryVersion (x_display,
					 &glx_major_version,
					 &glx_minor_version);
      GDK_GL_NOTE (MISC,
		   g_print ("GDK-GL-DEBUG: GLX version %d.%d\n",
			    glx_major_version,
			    glx_minor_version));
    }
  else
    {
      got_glx_version = False;
    }


  if (has_glx_extension != True)
    {
      g_warning ("Display '%s' has no GLX extension",
		 gdk_get_display ());
    }
  if (got_glx_version != True)
    {
      g_warning ("Can't get GLX extension version of Display '%s'",
		 gdk_get_display ());
    }


  if ((has_glx_extension == True) &&
      (got_glx_version   == True))
    {
      return TRUE;
    }
  else
    {
      return FALSE;
    }
}

gint
gdk_gl_set_current (GdkGLContext *gdk_gl_context,
		    GdkDrawable  *gdk_drawable)
{
  Display *x_display;
  Window   x_drawable;

  Bool r;

  g_return_val_if_fail (gdk_gl_context != NULL, FALSE);
  g_return_val_if_fail (gdk_drawable   != NULL, FALSE);


  x_display  = GDK_WINDOW_XDISPLAY (gdk_drawable);
  x_drawable = GDK_WINDOW_XWINDOW (gdk_drawable);

  r = glXMakeCurrent (x_display,
		      x_drawable,
		      glx_context_of_gdk_gl_context (gdk_gl_context));

  if (r)
    {
      gdk_gl_current_gdk_gl_context = gdk_gl_context;
      gdk_gl_current_gdk_drawable   = gdk_drawable;

      return TRUE;
    }
  else
    {
      return FALSE;
    }
}

void
gdk_gl_unset_current (void)
{
  gdk_gl_current_gdk_gl_context = NULL;
  gdk_gl_current_gdk_drawable   = NULL;
}

GdkGLContext *
gdk_gl_get_current_context (void)
{
  g_return_val_if_fail (gdk_gl_current_gdk_gl_context != NULL, NULL);


#ifdef G_ENABLE_DEBUG
  {
    GLXContext current_glx_context;
    GLXContext real_current_glx_context;

    current_glx_context
      = glx_context_of_gdk_gl_context (gdk_gl_current_gdk_gl_context);
    real_current_glx_context
      = glXGetCurrentContext ();

    if (current_glx_context != real_current_glx_context)
      {
	g_warning ("current_glx_context != real_current_glx_context\n");
	return NULL;
      }
  }
#endif /* G_ENABLE_DEBUG */

  return gdk_gl_current_gdk_gl_context;
}

GdkDrawable *
gdk_gl_get_current_drawable (void)
{
  g_return_val_if_fail (gdk_gl_current_gdk_drawable != NULL, NULL);


#ifdef G_ENABLE_DEBUG
  {
    Window current_x_drawable;
    Window real_current_x_drawable;

    current_x_drawable      = GDK_WINDOW_XWINDOW (gdk_gl_current_gdk_drawable);
    real_current_x_drawable = glXGetCurrentDrawable ();

    if (current_x_drawable != real_current_x_drawable)
      {
	g_warning ("current_x_drawable != real_current_x_drawable\n");
	return NULL;
      }
  }
#endif /* G_ENABLE_DEBUG */

  return gdk_gl_current_gdk_drawable;
}

void
gdk_gl_swap_buffers (GdkDrawable *gdk_drawable)
{
  Display      *x_display;
  Window       x_drawable;

  g_return_if_fail (gdk_drawable != NULL);


  x_display  = GDK_WINDOW_XDISPLAY (gdk_drawable);
  x_drawable = GDK_WINDOW_XWINDOW (gdk_drawable);

  glXSwapBuffers (x_display, x_drawable);
}

void
gdk_gl_flush_gdk (void)
{
  glXWaitX ();
}

void
gdk_gl_flush_gl (void)
{
  glXWaitGL ();
}
