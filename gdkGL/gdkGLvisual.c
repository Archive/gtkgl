/* gdkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gdk/gdkx.h>

#include <GL/gl.h>
#include <GL/glx.h>

#include "gdkGL.h"
#include "gdkGL_gdkutils.h"
#include "gdkGL_gdkxutils.h"


gint
gdk_gl_check_visual (GdkVisual *gdk_visual)
{
  gint is_available;
  int r;
  int val;
  XVisualInfo *x_visual_info;
  Display *x_display;
  
  g_return_val_if_fail (gdk_visual != NULL, FALSE);


  x_display = x_display_of_gdk_visual (gdk_visual);
  x_visual_info = x_visual_info_new_from_gdk_visual (gdk_visual);

  if ((x_display != NULL) &&
      (x_visual_info != NULL))
    {
      r = glXGetConfig (x_display, x_visual_info,
			GLX_USE_GL, &val);
      if ((r == 0) &&
	  (val == True))
	{
	  is_available = TRUE;
	}
      else
	{
	  is_available = FALSE;
	}
    }
  else
    {
      is_available = FALSE;
    }

  if (x_visual_info != NULL)
    g_free (x_visual_info);

  return is_available;
}

GList*
gdk_gl_list_visuals (void)
{
  GList *tmp;
  GList *all_visuals;
  GList *available_visuals;
  GdkVisual *gdk_visual;
  gint r;

  all_visuals = gdk_list_visuals ();
  available_visuals = NULL;

  tmp = all_visuals;
  while (tmp != NULL)
    {
      gdk_visual = tmp->data;
      tmp = g_list_next (tmp);

      r = gdk_gl_check_visual (gdk_visual);
      if (r == TRUE)
	available_visuals = g_list_append (available_visuals, gdk_visual);
    }

  g_list_free (all_visuals);

  return available_visuals;
}
