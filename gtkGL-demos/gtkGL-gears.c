/* gtkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <gtkGL/gtkGL.h>

#include "mesa-gears.h"

static struct {
    GLint    gear1, gear2, gear3;
    GLfloat  view_rotx, view_roty, view_rotz;
    GLfloat  angle;

    gint idle_tag;
} Gears;

static gint
draw_idle_callback (gpointer user_data)
{
  GtkWidget *widget;

  widget = (GtkWidget *)user_data;

  Gears.angle += 2.0;
  Gears.view_rotx += 0.15;
  Gears.view_roty += 0.5;
  Gears.view_rotz += 1.3;

  GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
    {
      mesa_gears_draw (Gears.gear1, Gears.gear2, Gears.gear3,
		       Gears.angle,
		       Gears.view_rotx, Gears.view_roty, Gears.view_rotz);
    }
  GTK_GL_WRAP_END;

  gtk_gl_swap_buffers_of_widget (widget);

  return TRUE;
}

static void
realize_callback (GtkWidget *widget,
		  gpointer   user_data)
{
  Gears.angle = 0.0;
  Gears.view_rotx = 20.0;
  Gears.view_rotx = 30.0;
  Gears.view_rotx = 0.0;


  GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
    {
      mesa_gears_init (&Gears.gear1, &Gears.gear2, &Gears.gear3);
    }
  GTK_GL_WRAP_END;

  Gears.idle_tag = gtk_idle_add (draw_idle_callback, (gpointer)widget);
}

static void
size_allocate_callback (GtkWidget     *widget,
			GtkAllocation *allocation,
			gpointer       user_data)
{
  gint width, height;

  if (GTK_WIDGET_REALIZED (widget))
    {
      width = allocation->width;
      height = allocation->height;
  
      GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
	{
	  mesa_gears_reshape (width, height);
	}
      GTK_GL_WRAP_END;
    }
  else
    {
      gtk_widget_queue_resize (widget);
    }
}

int
main(int    argc,
     char **argv)
{
  GtkWidget *window;
  GtkWidget *drawing_area;

  gtk_init (&argc, &argv);
  gtk_gl_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit),
		      NULL);
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (gtk_main_quit),
		      NULL);


  drawing_area = gtk_drawing_area_new();
  gtk_gl_adapt_widget (drawing_area);

  gtk_signal_connect (GTK_OBJECT (drawing_area), "realize",
		      GTK_SIGNAL_FUNC (realize_callback),
		      NULL);
  gtk_signal_connect (GTK_OBJECT (drawing_area), "size_allocate",
		      GTK_SIGNAL_FUNC (size_allocate_callback),
		      NULL);
  gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area),
			 100, 100);
  gtk_container_add (GTK_CONTAINER (window), drawing_area);

  gtk_widget_show_all (window);

  gtk_main();
  
  return 0;
}
