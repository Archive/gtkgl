/* gtkGL
 * Copyright (C) 1997,1998 AOSASA Shigeru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <gtkGL/gtkGL.h>

#include "mesa-gears.h"

static struct {
    GLint    gear1, gear2, gear3;
    GLfloat  view_rotx, view_roty, view_rotz;
    GLfloat  angle;

    gint idle_tag;
    gint timeout_tag;
} GearsPlus;

static void realize_callback (GtkWidget *, gpointer);
static void size_allocate_callback (GtkWidget *, GtkAllocation *, gpointer);
static void expose_event_handler (GtkWidget *, GdkEvent *, gpointer);

static void start_button_callback (GtkWidget *, gpointer);
static void stop_button_callback (GtkWidget *, gpointer);

static gint animation_callback (GtkWidget *);
static void draw_gears (GtkWidget *);


static void
start_button_callback (GtkWidget *widget,
		       gpointer   user_data)
{
  if (GearsPlus.idle_tag)
    {
      gtk_idle_remove (GearsPlus.idle_tag);
      GearsPlus.idle_tag = 0;
    }

  GearsPlus.idle_tag = gtk_idle_add ((GtkFunction)animation_callback,
				     GTK_WIDGET (user_data));
}

static void
stop_button_callback (GtkWidget *widget,
		      gpointer   user_data)
{
  if (GearsPlus.idle_tag)
    {
      gtk_idle_remove (GearsPlus.idle_tag);
      GearsPlus.idle_tag = 0;
    }
}

static gint
delayed_draw_2 (gpointer user_data)
{
  GtkWidget *widget;
    
  widget = GTK_WIDGET (user_data);

  if (GearsPlus.idle_tag)
    animation_callback (widget);
  else
    draw_gears (widget);

  GearsPlus.timeout_tag = 0;

  return FALSE;
}

static void
delayed_draw_1 (GtkWidget *widget)
{
  if (!GearsPlus.timeout_tag)
    {
      GearsPlus.timeout_tag
	= gtk_timeout_add(10, (GtkFunction)delayed_draw_2, (gpointer)widget);
    }
}

static void
expose_event_handler (GtkWidget *widget,
		      GdkEvent  *event,
		      gpointer   user_data)
{
  GdkEventExpose *expose_event;

  expose_event = (GdkEventExpose *)event;

  if (expose_event->count == 0)
    delayed_draw_1(widget);
}

static void
size_allocate_callback (GtkWidget     *widget,
			GtkAllocation *allocation,
			gpointer       user_data)
{
  gint width, height;

  if (GTK_WIDGET_REALIZED (widget))
    {
      width = allocation->width;
      height = allocation->height;
  
      GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
	{
	  mesa_gears_reshape(width, height);
	}
      GTK_GL_WRAP_END;
    }
  else
    {
      gtk_widget_queue_resize (widget);
    }
}

static void
realize_callback (GtkWidget *widget,
		  gpointer   user_data)
{
  GearsPlus.angle = 0.0;
  GearsPlus.view_rotx = 20.0;
  GearsPlus.view_rotx = 30.0;
  GearsPlus.view_rotx = 0.0;
  
  GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
    {
      mesa_gears_init(&GearsPlus.gear1,
		      &GearsPlus.gear2,
		      &GearsPlus.gear3);
    }
  GTK_GL_WRAP_END;
}

static void
draw_gears (GtkWidget *widget)
{
  GTK_GL_WRAP_BEGIN (gtk_gl_context_of_widget (widget))
    {
      mesa_gears_draw(GearsPlus.gear1,
		      GearsPlus.gear2,
		      GearsPlus.gear3,
		      GearsPlus.angle,
		      GearsPlus.view_rotx,
		      GearsPlus.view_roty,
		      GearsPlus.view_rotz);
    }
  GTK_GL_WRAP_END;

  gtk_gl_swap_buffers_of_widget (widget);
}

static gint
animation_callback (GtkWidget *widget)
{

  GearsPlus.angle += 2.0;

#if 0
  GearsPlus.view_rotx += 5.0;
  GearsPlus.view_roty += 0.5;
  GearsPlus.view_rotz += 0.15;
#elseif 0
  GearsPlus.view_rotz += 5.0;
  GearsPlus.view_roty += 0.5;
  GearsPlus.view_rotx += 0.15;
#else
  GearsPlus.view_rotz += 1.3;
  GearsPlus.view_roty += 0.5;
  GearsPlus.view_rotx += 0.15;
#endif

  draw_gears(widget);

  return TRUE;
}

int
main(int    argc,
     char **argv)
{
    GtkWidget *window;
    GtkWidget *drawing_area;

    GtkWidget *dialog;
    GtkWidget *quit_button;
    GtkWidget *start_button;
    GtkWidget *stop_button;

    /*
     *
     */

    gtk_init(&argc, &argv);
    gtk_gl_init (&argc, &argv);

    /*
     *
     */

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

    gtk_signal_connect (GTK_OBJECT(window), "destroy",
			GTK_SIGNAL_FUNC (gtk_main_quit),
			NULL);
    gtk_signal_connect (GTK_OBJECT(window), "delete_event",
			GTK_SIGNAL_FUNC (gtk_main_quit),
			NULL);

    drawing_area = gtk_drawing_area_new ();
    gtk_gl_adapt_widget (drawing_area);

    gtk_container_add (GTK_CONTAINER (window), drawing_area);
    gtk_drawing_area_size (GTK_DRAWING_AREA (drawing_area),
			   50, 50);

    gtk_signal_connect (GTK_OBJECT (drawing_area), "realize",
		        GTK_SIGNAL_FUNC (realize_callback),
			NULL);
    gtk_signal_connect (GTK_OBJECT (drawing_area), "size_allocate",
			GTK_SIGNAL_FUNC (size_allocate_callback),
			NULL);
    gtk_widget_set_events (drawing_area, GDK_EXPOSURE_MASK);
    gtk_signal_connect (GTK_OBJECT (drawing_area), "expose_event",
			GTK_SIGNAL_FUNC (expose_event_handler),
			NULL);

    gtk_widget_show_all (window);

    /*
     *
     */

    dialog = gtk_dialog_new ();

    gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
			GTK_SIGNAL_FUNC (gtk_main_quit),
			NULL);
    gtk_signal_connect (GTK_OBJECT (dialog), "delete_event",
			GTK_SIGNAL_FUNC (gtk_main_quit),
			NULL);

    quit_button = gtk_button_new_with_label ("Quit");
    start_button = gtk_button_new_with_label("Start");
    stop_button = gtk_button_new_with_label ("Stop");

    gtk_box_pack_start_defaults (GTK_BOX (GTK_DIALOG (dialog)->action_area),
				 quit_button);
    gtk_box_pack_start_defaults (GTK_BOX (GTK_DIALOG (dialog)->action_area),
				 start_button);
    gtk_box_pack_start_defaults (GTK_BOX (GTK_DIALOG (dialog)->action_area),
				 stop_button);

    gtk_signal_connect (GTK_OBJECT (quit_button), "clicked",
			GTK_SIGNAL_FUNC (gtk_main_quit),
			NULL);
    gtk_signal_connect_object (GTK_OBJECT (start_button), "clicked",
			       GTK_SIGNAL_FUNC (start_button_callback),
			       GTK_OBJECT (drawing_area));
    gtk_signal_connect_object (GTK_OBJECT (stop_button), "clicked",
			       GTK_SIGNAL_FUNC (stop_button_callback),
			       GTK_OBJECT (drawing_area));

    gtk_widget_show_all (dialog);

    /*
     *
     */

    gtk_main();

    return 0;
}
